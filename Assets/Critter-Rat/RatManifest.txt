Rat Critter version 1.0
by Rick Sternberg
http://www.maritimecathedral.com

To use:
Drag one of the prefabs (RatPrefab, RatIsoPrefab, or RatSidePrefab) into the scene and apply any scripting to them to have them animate. Each prefab has an Animator component with all the included actions added to it. The prefabs will default to the basic stand animation.

These are the files included with the Rat critter package:

3DRat Folder
-FBX Folder
--- RatBase.fbx		// Rigged rat model
--- RatDeath.fbx	// Death and Corpse animation for the rat model
--- RatIdleA.fbx	// Sniffing back and forth idle animation for the rat model
--- RatIdleB.fbx	// Rising up on hind legs and sniffing idle animation for the rat model
--- RatRun.fbx		// Running animation for the rat model
--- RatStand.fbx	// Default standing animation for the rat model
--- RatWalk.fbx		// walking animation for the rat model
- Materials Folder
--- Rat_Bump.psd	// A single layer PSD file containing the bump map
--- Rat_Color.psd	// A single layer PSD file containing the color map
--- Rat_Material	// A bumped diffuse material for the rat
- Rat3d_AnimController	// Animation controller used by the RatPrefab's Animator component. Contains all the above animations
- RatPrefab		// The scene ready 3D rat model 

IsoRat Folder
-RatDeath Folder
--- RatDeath64.png			// Isometric Sprite Sheet for the death and corpse animations using a 64x64 cell grid
--- RatIsoCorpse-E animation		// The corpse animation files in 8 directions
--- RatIsoCorpse-N animation
--- RatIsoCorpse-NE animation
--- RatIsoCorpse-NW animation
--- RatIsoCorpse-S animation
--- RatIsoCorpse-SE animation
--- RatIsoCorpse-SW animation
--- RatIsoCorpse-W animation
--- RatIsoDeath-E animation		// The death animation files in 8 directions
--- RatIsoDeath-N animation
--- RatIsoDeath-NE animation
--- RatIsoDeath-NW animation
--- RatIsoDeath-S animation
--- RatIsoDeath-SE animation
--- RatIsoDeath-SW animation
--- RatIsoDeath-W animation
-RatIsoIdleA Folder
--- RatIdleA64.png			// Isometric Sprite Sheet for the idleA animation using a 64x64 cell grid
--- RatIsoIdleA-E animation		// The sniffing back and forth idle animation files in 8 directions
--- RatIsoIdleA-N animation
--- RatIsoIdleA-NE animation
--- RatIsoIdleA-NW animation
--- RatIsoIdleA-S animation
--- RatIsoIdleA-SE animation
--- RatIsoIdleA-SW animation
--- RatIsoIdleA-W animation
-RatIsoIdleB Folder
--- RatIdleB64.png			// Isometric Sprite Sheet for the idleB animation using a 64x64 cell grid
--- RatIsoIdleB-E animation		// The rising up on hind legs and sniffing idle animation files in 8 directions
--- RatIsoIdleB-N animation
--- RatIsoIdleB-NE animation
--- RatIsoIdleB-NW animation
--- RatIsoIdleB-S animation
--- RatIsoIdleB-SE animation
--- RatIsoIdleB-SW animation
--- RatIsoIdleB-W animation
-RatIsoRun Folder
--- RatRun64.png			// Isometric Sprite Sheet for the running animation using a 64x64 cell grid
--- RatIsoRun-E animation		// The running animation files in 8 directions
--- RatIsoRun-N animation
--- RatIsoRun-NE animation
--- RatIsoRun-NW animation
--- RatIsoRun-S animation
--- RatIsoRun-SE animation
--- RatIsoRun-SW animation
--- RatIsoRun-W animation
-RatIsoStand Folder
--- RatStand64.png			// Isometric Sprite Sheet for the standing animation using a 64x64 cell grid
--- RatIsoStand-E animation		// The default standing animation files in 8 directions
--- RatIsoStand-N animation
--- RatIsoStand-NE animation
--- RatIsoStand-NW animation
--- RatIsoStand-S animation
--- RatIsoStand-SE animation
--- RatIsoStand-SW animation
--- RatIsoStand-W animation
-RatIsoWalk Folder
--- RatWalk64.png			// Isometric Sprite Sheet for the walking animation using a 64x64 cell grid
--- RatIsoWalk-E animation		// The walking animation files in 8 directions
--- RatIsoWalk-N animation
--- RatIsoWalk-NE animation
--- RatIsoWalk-NW animation
--- RatIsoWalk-S animation
--- RatIsoWalk-SE animation
--- RatIsoWalk-SW animation
--- RatIsoWalk-W animation
-RatIso-AnimController		// Animation controller used by the RatIsoPrefab's Animator component. Contains all the above 
-RatIsoPrefab			// The scene ready isometric rat sprite 

SideRat Folder
- RatDeathSide.png			// Sideview Sprite Sheet for the death and corpse animations using a 64x64 cell grid
- RatIdleASide.png			// Sideview Sprite Sheet for the sniffing back and forth animation using a 64x64 cell grid 
- RatIdlebSide.png			// Sideview Sprite Sheet for the rising up and sniffing animation using a 64x64 cell grid
- RatRunSide.png			// Sideview Sprite Sheet for the running animation using a 64x64 cell grid
- RatStandSide.png			// Sideview Sprite Sheet for the default standing animation using a 64x64 cell grid
- RatWalkSide.png			// Sideview Sprite Sheet for the walking animation using a 64x64 cell grid 
- RatCorpseSide animation		// Sideview corpse animation file
- RatDeathSide animation		// Sideview death animation file
- RatIdleASide animation		// Sideview sniffing back and forth animation file
- RatIdlebSide animation		// Sideview rising up and sniffing animation file
- RatRunSide animation		// Sideview running animation file
- RatStandSide animation		// Sideview default standing animation file
- RatWalkSide animation		// Sideview walking animation file
- RatSide-AnimController		// Animation controller used by the RatSidePrefab's Animator component. Contains all the above 
- RatSidePrefab			// The scene ready sideview rat sprite 

Source Files Folder
-RatCritter.zip
--- IsoSpriteRenders Folder		// Folder containing the rendered 64x64 sprite image files for the isometric rat in 8 directions for all the animations listed above
--- SideSpriteRenders Folder		// Folder containing the rendered 64x64 sprite image files for the sideview rat for all the animations listed above
--- Maya-3D Folder			
--- --- Scenes Folder
--- --- --- Rat.ma			// Contains the rigged rat model referenced in the animation files below
--- --- --- RatBase.ma			// Static rat model exported as RatBase.fbx
--- --- --- RatDeath.ma		// Death animation exported as RatDeath.fbx
--- --- --- RatIdleA.ma			// Standing and sniffing back and forth animation exported as RatIdleA.fbx
--- --- --- RatIdleB.ma			// Rising up on hind legs and sniffing idle animation exported as RatIdleB.fbx
--- --- --- RatRun.ma			// Running animation exported as RatRun.fbx
--- --- --- RatStand.ma		// Default Standing animation exported as RatStand.fbx
--- --- --- RatWalk.ma			// walking animation exported as RatWalk.fbx
--- --- --- IsoCamSetup.ma		// Camera and light setup used to render the Isometric views of the rat
--- --- --- SideCamSetup.ma		// Camera and light setup used to render the side view of the rat
--- --- Textures Folder
--- --- --- Rat.psd			// PSD contain multiple layers for both the bump and color maps of the rat
--- --- --- Rat_Bump.psd		// A single layer PSD file containing the bump map used in Unity
--- --- --- Rat_Color.psd		// A single layer PSD file containing the color map used in Unity
--- --- --- Rat_Bump.png		// A PNG file containing the bump map used in Maya
--- --- --- Rat_Color.png		// A PNG file containing the color map used in Maya


Version information:
Version 1.0:
Initial release

Version 1.01
Updated the Rat_Material to 2017.2 Standard Shader
Improved the quality of the normal texture
FBX files moved to a separate folder





