﻿
===== Apocalyptic City Pack  =====


1. Intro
2. Content
3. How to use
4. Support

1. -=Intro=-
Thank you for purchasing the Apocalyptic City Pack!
You are free to use the models in your own project in any way you like.
However it is recommended to use them as static background assets, 
All models are used and and works with Unity free and Pro. We hope that you enjoy this amazing Apocalyptic CIty Pack.

2. -=Content=-
This pack contains a total of 854 unique models. Which can be divided in several categories:


Extreme Home Furniture Pack (contains 251 models)
For details see: https://www.assetstore.unity3d.com/#/content/13786

Extreme Flora Pack (contains 63 models)
For details see: https://www.assetstore.unity3d.com/#/content/8622

For details see: https://www.assetstore.unity3d.com/#/content/8709
Extreme Street Pack (contains 95 models)

For details see: https://www.assetstore.unity3d.com/#/content/8344
Extreme Vehicle Pack (contains 60 models)

Extreme Crate Pack (contains 52 models)
For details see: https://www.assetstore.unity3d.com/#/content/8668

Extreme Road Pack  (contains 133 models)
For details see: https://www.assetstore.unity3d.com/#/content/10471

Extreme Building Pack (contains 200 models)
For details see: https://www.assetstore.unity3d.com/#/content/10724

3. -=How to use=-

=Placing=
Simply place the assets from Extreme (Pack name)\Models into your scene of choice. Al textures and materials are linked. 
You can apply a different shader or diffuse color, which you can edit in Extreme (pack name)\Models\Model_....\Materials.

=LOD Group=
Some models contain a LOD Group. This can only be used and seen in Unity Pro. To remove the LOD Group, just simply remove the LOD Group component and the models with names ending with …_LOD1 and …_LOD2.


4. -=Support=-	
For more information or questions, please send your email to info@vertigo-games.com.



 Vertigo Games B.V.

