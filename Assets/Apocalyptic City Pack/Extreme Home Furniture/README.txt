﻿
===== Extreme Home Furniture Pack =====


1. Intro
2. Content
3. How to use
4. Support

1. -=Intro=-
Thank you for purchasing the Home Furniture Pack!
You are free to use the models in your own project in any way you like.


2. -=Content=-
This package contains 60 models. 


3 banners
4 beds
1 beer bottle
1 bench
12 kitchen tools
1 bible
1 bin
1 blanket
4 blinds
1 boiler
8 books
6 book sets
1 bowl
1 buggy
1 cabinet
2 cans
3 candles
12 candle holders
1 chandelier
6 carpets
10 chairs
4 cloth hangers
1 cloth lap
1 cloth role
2 coat racks
1 coffee cup
1 coffee machine
8 couches + extras
1 old school monitor
4 cubboards
5 curtains + holder
1 decagramme
2 desks
1 diving board
1 dresser
1 electric guitar
1 fax
10 fitness devices
1 flute
1 plant foilage
1 food tray
2 food baskets
1 fridge
3 garbage bags
1 guitar + snares
1 hammer
1 handdryer
1 toilet
8 kitchen pans
2 kitchen steps
1 laundromat
3 baskets with baby
1 little table
1 lod chair
2 matresses
1 microwave
1 mirror
8 beds
7 rubble cloth pieces
22 paintings + 2 frames
12 newspaper litter
1 paperholder
1 payphone
1 paperplate
1 phone
8 photoframes
1 piano
4 pillows
6 pots
1 retro keyboard
1 scissor
1 screwdriver
1 sewingmachine
1 sink
2 slippers
1 dummy face
2 stools
2 outside swings
7 tables
2 table cloths
1 theapot
1 teddybear
2 TL lights
1 toilet
1 toolbox with extras
1 toy horse
1 trolley
1 vending machine
1 voilin with snares
1 waterbowl
1 work stool
2 work tables
1 wrench

















3. -=Support=-	
For more information or questions, please send your email to info@vertigo-games.com.


 Vertigo Games B.V.



