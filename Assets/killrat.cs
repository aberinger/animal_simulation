﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class killrat : MonoBehaviour {
    public bool death;
    public GameObject rat;
    public bool poisoned = false;
    private float timeSinceDeath = 0;
	// Use this for initialization
	void Start () {
        GetComponent<Animator>().SetBool("running", true);
	}
	
	// Update is called once per frame
	void Update () {
        if (!death && (transform.position - GameManager.player.position).sqrMagnitude < 2.5)
        {
            GameManager.player.GetComponent<PlayerBehaviour>().eatRat(poisoned);
            death = true;
            GetComponent<Animator>().SetBool("running", false);
            GetComponent<NavMeshAgent>().speed = 0;
            GetComponent<ParticleSystem>().Play();
            Destroy(rat, 2f);
        }
        if (death)
        {
            timeSinceDeath += Time.deltaTime;
            transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, Mathf.Lerp(0, 180, timeSinceDeath));
            transform.position += new Vector3(0, Mathf.Lerp(0, .2f, timeSinceDeath), 0);
            //enabled = false;
            //GameObject.Destroy(rat);
        }
	}
}
