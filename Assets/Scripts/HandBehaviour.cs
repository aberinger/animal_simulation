﻿using NewtonVR;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;

public class HandBehaviour : MonoBehaviour {

    public bool menuHand;

    private Canvas menu;
    private PlayerBehaviour parent;
    private bool colliding = false;
    private Collider ground = null;
    private HandBehaviour otherHand;

    public float moveSpeed
    {
        get
        {
            return parent.speed;
        }
    }
    static List<HandBehaviour> hands = new List<HandBehaviour>();
    public static HandBehaviour handOnGround = null;
    private Vector3 prevPos;
    private bool doOnce = false;
	// Use this for initialization
	void Start () {
        parent = transform.parent.GetComponent<PlayerBehaviour>();
        if (menuHand)
            menu = GetComponentInChildren<Canvas>();
        if (hands.Count == 0)
        {
            hands.Add(this);
        } else
        {
            hands[0].otherHand = this;
            otherHand = hands[0];
            hands.Add(this);
        }
        /*
        foreach (Transform c in transform) {
            c.GetChild.radius = 0.05f;
        }*/
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (menuHand)
            menu.enabled = GetComponent<NVRHand>().UseButtonPressed;

        if (!doOnce)
        {
            bool modified = false;
            foreach (Transform c in transform)
            {
                SphereCollider coll = c.GetComponent<SphereCollider>();
                if (coll == null)
                    continue;
                modified = true;
                coll.radius = 0.08f;
            }

            if (modified)
                doOnce = true;
        }
        if (GameManager.player.GetComponent<PlayerBehaviour>().dead)
        {
            return;
        }
        if (handOnGround == null)
        {
            transform.parent.position -= new Vector3(0, 2f * Time.deltaTime, 0);
        }
		if (handOnGround == this)
        {
            Vector3 delta = transform.position - prevPos;
            transform.root.position -= Vector3.Scale(delta, new Vector3(moveSpeed, 1, moveSpeed));
            //transform.root.position -= new Vector3(delta.x * 2, delta.y, delta.z * 2);
            Vector3 temp = prevPos;
            prevPos = transform.position;
            transform.position = temp - delta;
        }
	}
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.parent != null && other.transform.parent.GetComponent<HandBehaviour>() != null)
            return;
        RaycastHit hit;
        if (Physics.Raycast(transform.position, new Vector3(0, -1, 0), out hit, .3f))
        {
            //Debug.Log(hit.normal);
            if (hit.normal.y < 0.8)
                return;
        }
        else
            return;
        if (handOnGround != this)
        {
            AudioSource a = GetComponent<AudioSource>();
            a.Play();
        }
        handOnGround = this;
        prevPos = transform.position;
        colliding = true;
        ground = other;
    }

    private void OnTriggerExit(Collider other)
    {
        if (ground != null || ground == other)
        {
            ground = null;
            colliding = false;
            if (otherHand.colliding)
            {
                handOnGround = otherHand;
            }
        }
        /*
        if (other.transform.parent.GetComponent<HandBehaviour>() != null)
            return;
        handOnGround = 
        */
    }
}
