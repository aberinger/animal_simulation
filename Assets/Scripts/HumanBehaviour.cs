﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class HumanBehaviour : MonoBehaviour {


    // Cooldown during which the poisoned chance isn't increased if the player is seen again
    public float SawPlayerCooldown;

    private NavMeshAgent agent;
    private Animator anim;
    private Transform dest;
    private List<Transform> waypoints;

    private float SawPlayer = 0;

    public bool CanSeePlayer()
    {
        var player = GameManager.player;
        Vector3 origin = transform.position + new Vector3(0, 1.5f, 0);
        Vector3 dir = ((player.position + new Vector3(0, 1, 0)) - origin);
        if (Vector3.Angle(transform.forward, dir) > 60)
            return false;
        //Ray r = new Ray(origin, dir);
        //Debug.DrawRay(origin, dir, Color.red, 10f);
        //Debug.DrawLine(origin, player.position + new Vector3(0, 1, 0), Color.red, 1f);
        RaycastHit hit;
        if (Physics.Raycast(origin, dir, out hit, 200))
        {
            if (hit.transform.root.tag == "Player")
                return true;
        }
        return false;
    }

	// Use this for initialization
	void Start () {
        waypoints = Waypoints.HumanWaypoints;
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        dest = waypoints[Random.Range(0, waypoints.Count)];
        agent.SetDestination(dest.position);
        anim.SetBool("Walking", true);
    }
	
	// Update is called once per frame
	void Update () {
		if ((transform.position - dest.position).sqrMagnitude < 5)
        {
            dest = waypoints[Random.Range(0, waypoints.Count)];
            agent.SetDestination(dest.position);
        }
        SawPlayer -= Time.deltaTime;
        if (SawPlayer <= 0 && CanSeePlayer())
        {
            SawPlayer = SawPlayerCooldown;
            GameManager.PoisonedChance *= 1.1f;
            Debug.Log("seeing player, poisoned chance up to " + GameManager.PoisonedChance);
        }
	}
}
