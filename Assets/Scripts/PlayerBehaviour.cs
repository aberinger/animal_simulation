﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;
using UnityEngine.UI;

public class PlayerBehaviour : MonoBehaviour {

    bool poisoned;
    public Slider slider;
    public float speed;
    public float InitialPoisonedDuration;
    public float InitialNourishment;
    public float hungerRate;
    float poisonedDuration;
    float rot = 0;
    float initSpeed;
    private VignetteAndChromaticAberration chr;
    float nourishment;
    public bool dead = false;
    
    
    private void Awake()
    {
        GameManager.player = transform;
    }

    // Use this for initialization
    void Start () {
        initSpeed = speed;
        chr = Camera.main.GetComponent<VignetteAndChromaticAberration>();
        nourishment = InitialNourishment;
	}
	 
	// Update is called once per frame
	void Update ()
    {
       
        if (dead)
            return;
        nourishment -= hungerRate * Time.deltaTime;
        if (nourishment <= 0)
        {
            Debug.Log("Player starved to death");
            dead = true;
            return;
        }
        //Debug.Log(nourishment);
        rot = Mathf.Lerp(rot, Input.GetAxis("Oculus_GearVR_RThumbstickY"), .05f);
        if (rot != 0 && HandBehaviour.handOnGround != null)
            transform.RotateAround(HandBehaviour.handOnGround.transform.position, Vector3.up, -90 * rot * Time.deltaTime);
        //transform.Rotate(new Vector3(0, -60 * rot * Time.deltaTime, 0));
        if (poisoned)
        {
            speed = Mathf.Lerp(speed, Mathf.Max(initSpeed / (1 + poisonedDuration / 20), 1), Time.deltaTime);
            speed = Mathf.Min(initSpeed, speed);
            chr.chromaticAberration = Mathf.Lerp(chr.chromaticAberration, Mathf.Min(poisonedDuration * 20, 60), 1f * Time.deltaTime);
            poisonedDuration -= Time.deltaTime;
        }
        if (poisonedDuration < 0)
        {
            //speed = initSpeed;
            poisoned = false;
        }
        slider.value = nourishment / InitialNourishment;
	}

    public void eatRat(bool poisoned)
    {
        if (poisoned)
        {
            this.poisoned = true;
            poisonedDuration += InitialPoisonedDuration;
            //speed = 1.2f;
            Debug.Log("Poisoned!");
        } else
        {
            if (nourishment + 20 <= InitialNourishment)
            {
                nourishment += 20;
            }
            else
            {
                nourishment = InitialNourishment;
            }
        }
    }
}
