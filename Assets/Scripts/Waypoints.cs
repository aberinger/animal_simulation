﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AgentType
{
    Rat, Human, Dog, Fox
}

public class Waypoints : MonoBehaviour {

    public static List<Transform> HumanWaypoints = new List<Transform>();
    public static List<Transform> RatWaypoints = new List<Transform>();
    public static List<Transform> DogWaypoints = new List<Transform>();
    public static List<Transform> FoxWaypoints = new List<Transform>();

    public AgentType agentType;

    public static Transform RandomWaypoint(AgentType type)
    {
        List<Transform> waypoints;
        switch (type)
        {
            case AgentType.Dog:
                waypoints = DogWaypoints;
                break;
            case AgentType.Human:
                waypoints = HumanWaypoints;
                break;
            case AgentType.Rat:
                waypoints = RatWaypoints;
                break;
            case AgentType.Fox:
                waypoints = FoxWaypoints;
                break;
            default:
                return null;
        }
        return waypoints[Random.Range(0, waypoints.Count)];
    }

	// Use this for initialization
	void Awake () {
        List<Transform> points = new List<Transform>();
        foreach (Transform t in transform)
            points.Add(t);
		switch (agentType)
        {
            case AgentType.Dog:
                DogWaypoints.AddRange(points);
                break;
            case AgentType.Human:
                HumanWaypoints.AddRange(points);
                break;
            case AgentType.Rat:
                RatWaypoints.AddRange(points);
                break;
            case AgentType.Fox:
                FoxWaypoints.AddRange(points);
                break;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
