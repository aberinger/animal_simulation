﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FoxBehavior : MonoBehaviour {

    public Transform dest = null;
    NavMeshAgent agent;
    Animator anim;

    int aggressive = Animator.StringToHash("Aggressive");
    int running = Animator.StringToHash("Running");
    int bite = Animator.StringToHash("Bite");
    float biteCD = 0;

    float playerDist()
    {
        return (GameManager.player.position - transform.position).sqrMagnitude;
    }

    float destDist()
    {
        if (dest == null)
            return float.PositiveInfinity;
        return (dest.position - transform.position).sqrMagnitude;
    }

	// Use this for initialization
	void Start () {
        dest = Waypoints.FoxWaypoints[Random.Range(0, Waypoints.FoxWaypoints.Count)];
        agent = GetComponent<NavMeshAgent>();
        agent.SetDestination(dest.position);
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        float dist = playerDist();
        biteCD -= Time.deltaTime;
		if (dist > 100) // 10m
        {
            agent.speed = .1f;
            anim.SetBool(aggressive, false);
            anim.SetBool(running, false);
            if (destDist() < 2)
            {
                dest = Waypoints.FoxWaypoints[Random.Range(0, Waypoints.FoxWaypoints.Count)];
                agent.SetDestination(dest.position);
            }
        } else if (dist > 25) // 5m
        {
            agent.speed = 0;
            float y = transform.rotation.eulerAngles.y;
            transform.LookAt(GameManager.player);
            transform.rotation = Quaternion.Euler(0, Mathf.Lerp(y, transform.rotation.eulerAngles.y, .05f), 0);
            anim.SetBool(aggressive, true);
        } else
        {
            if (dist < 9)
            {
                if (biteCD <= 0)
                {
                    biteCD = 2;
                    agent.speed = 0;
                    anim.SetTrigger(bite);
                } else
                {
                    anim.SetBool(running, false);
                    anim.SetBool(aggressive, true);
                    agent.speed = 0f;
                }
            }
            else
            {
                agent.speed = .1f;
                agent.SetDestination(GameManager.player.position);
                anim.SetBool(running, true);
            }
        }
	}
}
