﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class ratspawnmvt : MonoBehaviour {
    public GameObject rat;
    public Transform[] spawnpoints;
    public float InitialPoisonChance;
    protected int i = 0;
    public float spawncd;
    private float timeStamp=0;
    private GameObject currat;
    private float poisonedChance
    {
        get
        {
            return GameManager.PoisonedChance;
        }
    }
	// Use this for initialization
	void Start () {
        //poisonedChance = InitialPoisonChance;
	}
	
	// Update is called once per frame
	void Update () {
        if (timeStamp <= Time.time)
        {
            if (i < spawnpoints.Length)
            {
                currat = Object.Instantiate(rat, spawnpoints[i].position, Quaternion.identity);
                //Debug.Log("Rat created at" + currat.transform.position);
                //currat = Instantiate(rat);
                //currat.transform.position = spawnpoints[i].position;
                /*
                if (1 + i < spawnpoints.Length)
                {
                   currat.GetComponent<NavMeshAgent>().SetDestination(spawnpoints[i + 1].position);
                }
                else {
                    currat.GetComponent<NavMeshAgent>().SetDestination(spawnpoints[0].position);
                }
                */
                currat.name = "rat" + i;
                i++;
                timeStamp = Time.time + spawncd;
                if (Random.Range(0f, 1f) < poisonedChance)
                {
                    currat.GetComponent<killrat>().poisoned = true;
                }
            }
            else { i = 0; }
            timeStamp = Time.time + spawncd;
        }
    }
    
}
