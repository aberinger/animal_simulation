﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MySceneManager : MonoBehaviour {
    
    // Use this for initialization
    void Start () {
        
    }
   
    // Update is called once per frame
    void Update () {
		
	}
    

    public void LoadMainScene()
    {
        SceneManager.LoadScene("Main Scene");
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
   
    
  

    
       
    

    public void ReduceAudio()
    {
        AudioSource[] audioSources = Object.FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
        foreach (AudioSource audio in audioSources)
        {
            audio.volume -= 0.1f;
        }
    }
    public void IncreaseAudio()
    {
        AudioSource[] audioSources = Object.FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
        foreach (AudioSource audio in audioSources)
        {
            audio.volume += 0.1f;
        }
    }
}
