﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FemaleFoxBehavior : MonoBehaviour
{

    public Transform dest = null;
    NavMeshAgent agent;
    Animator anim;
    
    int running = Animator.StringToHash("Running");

    float playerDist()
    {
        return (GameManager.player.position - transform.position).sqrMagnitude;
    }

    float destDist()
    {
        if (dest == null)
            return float.PositiveInfinity;
        return (dest.position - transform.position).sqrMagnitude;
    }

    // Use this for initialization
    void Start()
    {
        dest = Waypoints.FoxWaypoints[Random.Range(0, Waypoints.FoxWaypoints.Count)];
        agent = GetComponent<NavMeshAgent>();
        agent.SetDestination(dest.position);
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (playerDist() > 100) // 10m
        {
            //agent.speed = .1f;
            //anim.SetBool(running, false);
            if (destDist() < 2)
            {
                dest = Waypoints.FoxWaypoints[Random.Range(0, Waypoints.FoxWaypoints.Count)];
                agent.SetDestination(dest.position);
            }
        }/*
        else
        {
            agent.speed = .1f;
            agent.SetDestination(GameManager.player.position);
            anim.SetBool(running, true);
        }*/
    }
}
