﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RatBehaviour : MonoBehaviour {

    private NavMeshAgent agent;
    private Transform dest;
    
	// Use this for initialization
	void Start () {
        agent = GetComponent<NavMeshAgent>();
        dest = Waypoints.RandomWaypoint(AgentType.Rat);
        agent.SetDestination(dest.position);
	}
	
	// Update is called once per frame
	void Update () {
		if ((dest.position - transform.position).sqrMagnitude < 1)
        {
            dest = Waypoints.RandomWaypoint(AgentType.Rat);
            agent.SetDestination(dest.position);
        }
	}
}
